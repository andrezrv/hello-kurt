=== Hello Kurt ===
Contributors: andrezrv
Tags: hello, kurt, nirvana, dolly, admin, random
Requires at least: 3.0
Tested up to: 3.5.2
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

When activated you will randomly see a lyric from Smells Like Teen Spirit in the upper right of your admin screen on every page.

== Description ==

Just as [Hello Dolly](http://wordpress.org/plugins/hello-dolly/), this is not just a plugin, it symbolizes the angst and disconformity that eventually lead to the hope and enthusiasm of an entire generation wanting to make a change and mark a difference. When activated you will randomly see a lyric from Smells Like Teen Spirit in the upper right of your admin screen on every page.

This is a totally rip off from Matt Mullenweg's [Hello Dolly plugin](http://wordpress.org/plugins/hello-dolly/), so all credits should go to him.

== Installation ==

1. Unzip `hello-kurt.zip` and upload the `hello-kurt` folder to your `/wp-content/plugins/` directory.
2. Activate the plugin through the **"Plugins"** menu in WordPress.

== Changelog ==

= 1.0 =
First public release!
